#ifndef DSBCONT_H
#define DSBCONT_H

#include <string>
#include <iostream>
#include <cstdlib>
#include "item.h"
#include "Container.h"

using namespace std;

class dsbCont : public Container{
	
	public:
	
	dsbCont()
	:Container()
	{
		isAddable = false;
		
	}
	dsbCont(string _invName)
	:Container(_invName)
	{
		isAddable = false;
		
	}
	dsbCont(string _invName,string _invType)
	:Container(_invName,_invType)
	{
		isAddable = false;
		
	}
	dsbCont(string _invName,string _invType,string _invDesc)
	:Container(_invName,_invType,_invDesc)
	{
		isAddable = false;
		
	}
	dsbCont(string _invName,string _invType,string _invDesc,item* inv[])
	:Container(_invName,_invType,_invDesc,inv)
	{
		isAddable = false;
		
	}
	dsbCont(string _contName,string _contType,string _contDesc, item* hidItem)
	:Container(_contName,_contType,_contDesc,hidItem)
	{
		isAddable = false;
	}
	bool getIsAddable(){
		return isAddable;
	}
	
	
	
	private:
		bool isAddable;
};
#endif
