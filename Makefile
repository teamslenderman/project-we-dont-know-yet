XX = g++
CXXFLAGS = -Wall -ansi -pedantic -g -O0

all: main

clean: 
	rm -f *.o main

inventory.o: inventory.h Container.h
	$(CXX) $(CXXFLAGS) -c inventory.h Container.h
player.o: player.cpp player.h inventory.o
	$(CXX) $(CXXFLAGS) -c player.cpp
	
monster.o: monster.cpp monster.h
	$(CXX) $(CXXFLAGS) -c monster.cpp
	
agent.o: agent.cpp agent.h
	$(CXX) $(CXXFLAGS) -c agent.cpp

room.o: room.cpp room.h
	$(CXX) $(CXXFLAGS) -c room.cpp

main.o: main.cpp item.h Container.h
	$(CXX) $(CXXFLAGS) -c main.cpp item.h Container.h

main: main.o room.o agent.o player.o monster.o
	$(CXX) $(CXXFLAGS) -o main main.o room.o agent.o player.o monster.o
