READ ME. add comments for general discussion

<simon> Sorry i am pretty busy too. I added a very basic implemantation for the agent. what i will do tomorrow: add monster and player classes. check if everything is errorfree and compilable, adapt the makefile. We should talk about what the act() method does (do we include fighting with lifepoints or whatever and how does the player interact with doors, items etc for example). 
good night </simon>

Hey guys, I will get workin later tonight. Been really busy but will probably be done with containers by tomorrow ish. How is everyone else doing?

I'm working on randomly linking rooms. i removed the .o and files that ended
with ~ because that is what we went over in class today. so now the repository
has the .h and .cpp files only.

<george>
simon, i was thinking that the act() method should ask the user what he would
like to do then processes it. main() would iterate through the agents->act()
just keeping track of whose turn it is. i don't think we should have fighting
or life points. if the ghost enters a room with one of the players in it then
on that players turn, he will role die to see if he lives and moves to the
next room. 
i was thinking the player interaction would go something like this.....
player1>look around the room<enter>
Looks like you're in the library. there is a bookshelf with books, and a
neatly organized desk. The library has one door to the east and one to the
north.
player1>check out the desk<enter>
there is a key in the drawer of the desk.
player1>pick up the key<enter>
key added to your bag
player1>check the door to the north<enter>
the door is unlocked, and the rooms appears to be the office.
player1>move to the north<enter>
you are now in the office.
....
or something like that. that's what i was think the interaction would be. in
main.cpp, i added a method that tokenizes the sentence that the user inputs so
that can be copied over to act(). i think that would mean that the player
would need a pointer to what room he is in so that he can move rooms. i don't
just some thoughts of how i thought it was going to work. what do yall think?
</george>


Hey all I need some help inside the player.cpp class.... my implementation of some of our commands is running into an array out of bounds (I think) because it crashes when I try to use an iterator for the set<Container*> return from room.cpp
Other than that what do you guys think so far?
</Brandon>