#include "monster.h"
#include <string>

using namespace std;

Monster::Monster(){
}

/**
 * Constructor that sets all the properties and puts the monster in a starting room
 */
Monster::Monster(string name, string description, Room* location){
	this->name=name;
	this->description=description;
	this->location=location;
	location->enter(this);

}

/**
 * Destructor
 */
Monster::~Monster(){
	//TODO
}
	
/**
 * Method that moves the monster around randomly
 */	
bool Monster::act(){
	int n = rand() % 4;
	string direction;
	switch(n)
	{
    	case 0: direction = "north"; break;
    	case 1: direction = "south"; break;
    	case 2: direction = "west"; break;
    	case 3: direction = "east"; break;
	}
	if(location->getLinked(direction) != NULL)
	{
    location->leave(this);
    location = location->getLinked(direction);
    location->enter(this);
	}
	
}
