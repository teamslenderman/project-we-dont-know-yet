#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <map>
#include <set>
#include "dsbCont.h"
using namespace std;

class Agent;
class Container;

/**
 * Class Room - .h-file
 *
 * Room represents the single rooms and has diferent properties and methods
 */

class Room{
	public:
		Room();
		Room(string name, string description);
		~Room();
		string getName();
		string getDescription();
		Room *getLinked(string direction);
		void printLinked();
		void link(Room* r, string dir);
		void setLocked(bool flag);
		bool isLocked();
		void setDark(bool flag);
		bool isDark();
		void setExit(bool flag);
		bool isExit();
		bool isEqual(Room* r);
		void setDesc(string desc);
		
 		set<Agent*> getOccupants();
 		set<Container*> getRoomObjs();
 		void addOccupant(Agent* a);
 		void addContainer(Container* obj);
		void enter(Agent* a);
 		void leave(Agent* a);
 		void printOccupants();

	private:
		string name;
		string description;
		map<string, Room*> adj_rooms;
		set<Agent*> occupants;
		set<Container*> room_objs;
		bool locked;
		bool dark;
		bool exit;
};
#endif
