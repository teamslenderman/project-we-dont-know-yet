#include "agent.h"
#include <string>
/**
 * Class Agent - .cpp-file
 *
 * Template for Monster and Player
 *
 */
class Room;

/*
 * Getters and Setters
 */
string Agent::getName(){
	return this->name;
}

string Agent::getDescription(){
	return this->description;
}

Room* Agent::getLocation(){
	return this->location;
}

void Agent::setName(string name){
	this->name = name;
}

void Agent::setDescription(string description){
	this->description = description;
}

void Agent::setLocation(Room* location){
	this->location = location;
}

bool Agent::act(){
}
