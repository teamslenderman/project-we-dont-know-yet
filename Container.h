#ifndef CONTAINER_H
#define CONTAINER_H

#include "item.h"
#include <string>
#include <cstdlib>
using namespace std;

/**
 * Class Container
 * 
 * a class that can hold and operate on items
 *
 */
class Container{
	
	public:
	
/**
 * Empty Constructor 
 */	
	Container(){
	name = "unknown";
	type = "unknown";
	description = "unkown object to us and completely irrelevant";
	for(int i = 0; i<5;i++){
		items[i] = NULL;
	}
	count = 0;
	
	}
/**
 * Constructor that only knows a name
 */
	Container(string _contName){
	name = _contName;
	type = "unknown";
	description = "unkown object to us and completely irrelevant";
	for(int i = 0; i<5;i++){
		items[i] = NULL;
	}
	count = 0;
	
	}
/**
 * Constructor with name and type
 */
	Container(string _contName,string _contType){
	name = _contName;
	type = _contType;
	description = "This object is indescribable";
	for(int i = 0; i<5;i++){
		items[i] = NULL;
	}
	count = 0;
	}

/**
 * Constructor with name, type and description
 */
	Container(string _contName,string _contType,string _contDesc){
	name = _contName;
	type = _contType;
	description = _contDesc;
	for(int i = 0; i<5;i++){
		items[i] = NULL;
	}
	count = 0;
	
	}

/**
 * Constructor with name, type, description and an item array
 */
	Container(string _contName,string _contType,string _contDesc, item* inv[]){
	name = _contName;
	type = _contType;
	description = _contDesc;
	for(int i = 0; i<5;i++){
		items[i] = inv[i];
	}
	count = (sizeof(inv)/sizeof(item*));
	
	}
	Container(string _contName,string _contType,string _contDesc, item* hidItem){
	name = _contName;
	type = _contType;
	description = _contDesc;
	items[0] = hidItem;
	count = 1;
	
	}
	
		/* GETTERS */
		
		string getName(){
			return name;
		}
		string getType(){
			return type;
		}
		string getDescription(){
			return description;
		}

/**
 * Outputs all the items in the container
 */
		void seeItems(){
			for(int i = 0; i<5;i++){
				if(items[i] == NULL){
					
				}
			else{
				cout << items[i]->getName() << endl;
			}
			
			}
		}

/**
 * adds item to container
 */
		void addItem(item *tbAdded){
			if(tbAdded == NULL){
			}else{
				if( count >= 5){
					cout<<"Bag is Full sorry"<<endl;
					cout<<endl;
				}else{
					for(int i = 0; i<5;i++){
						if(items[i] == NULL){
							items[i] = tbAdded;
							count++;
							break;
						}
					}
				}
			}
		}
/**
 * removes item from container, returns false if item not in container
 */
		bool removeItem(string tbRemoved){
		for(int i = 0; i<5;i++){
				if(items[i] !=NULL && items[i]->getName() == tbRemoved){
					items[i] = NULL;
					--count;
					return true;
				}
		}
		return false;
		}

/**
 * returns wheter container is empty or not
 */
		bool isEmpty(){
			/*
			int count = 0;
			for(int i = 0; i<5;i++){
				if(items[i] = NULL){
					count++;
				}
				
			}
			if(count == 5){
				return true;
			}
			else{
				return false;
			}
			*/
			return count == 0;
		}	

/**
 * returns true if all the slots in the container are used
 */
		bool noRoom(){
			for(int i = 0; i<5;i++){
				if(items[i] = NULL){
					return false;
				}
			}
			return true;
		}

/**
 * uses an item in the container and removes it afterwards
 */
		void use(item *tbUse){
			if(tbUse->getType() == "map"){
				cout<<tbUse->getDesc();
				this->removeItem(tbUse->getName());
			}
			else if(tbUse->getType() == "flashlight"){
				cout<<tbUse->getDesc();
				this->removeItem(tbUse->getName());
			}
		}
		
/**
 * returns an item from the container
 */
		item* pickUpItem(string iName){
			for(int i = 0; i<5;i++){
				if(items[i]->getName() == iName){
					return items[i];	
				}
			}
			cout<< "No such item to pick up"<<endl;
			return NULL;
		}
		
		
		
	private:
		string name;
		string type;
		string description;
		item *items[5];
		int count;
		
		
	
	
	
	
};
#endif
