#include "room.h"
#include <iostream>

class Agent;
class Container;

/**
 * empty constructor
 */
Room::Room(){
	locked = false;
	dark = false;
	exit = false;
}

/**
 * constructor that sets name and description
 */
Room::Room(string name, string description){
	this->name = name;
	this->description = description;
	locked = false;
	dark = false;
	exit = false;
}

/**
 * Destructor
 */
Room::~Room(){
}


/**
 * returns room description plus the ways to the next rooms 
 */
string Room::getDescription(){
	string desc = this->description;
	if(getLinked("north") != NULL){
		desc += "\nThere is a door to the North";
		cout<<endl;
	}
	if(getLinked("south") != NULL){
		desc += "\nThere is a door to the South";
		cout<<endl;
	}
	if(getLinked("east") != NULL){
		desc += "\nThere is a door to the East";
		cout<<endl;
	}
	if(getLinked("west") != NULL){
		desc += "\nThere is a door to the West";
		cout<<endl;
	}
	return desc;
}

/**
 * links one room to another one in the direction dir
 */
void Room::link(Room *r, string dir){
	if(getLinked(dir) == NULL)
	{
		this->adj_rooms[dir] = r;
	}
}

/**
 * gets the room in one direction
 */
Room* Room::getLinked(string direction){
	return this->adj_rooms[direction];
}

/**
 * prints the map
 */
void Room::printLinked(){
	
	for(map<string, Room*>::iterator it = adj_rooms.begin(); it != adj_rooms.end(); ++it){
		cout << it->first << endl;
		cout << it->second->getName() << endl;
		cout << it->second->getDescription() << endl;
	}
}


/*  GETTERS + SETTERS */
string Room::getName(){
        return this->name;
}

void Room::setLocked(bool flag){
	this->locked = flag;
}

bool Room::isLocked(){
	return this->locked;
}

void Room::setDark(bool flag){
	this->dark = flag;
}

bool Room::isDark(){
	return this->dark;
}

void Room::setExit(bool flag){
	this->exit = flag;
}

bool Room::isExit(){
	return this->exit;
}

/**
 * cchecks if two rooms are the same
 */
bool Room::isEqual(Room* r){
	bool flag = true;
	if(this->name.compare(r->getName()) != 0){
		flag = false;
	}else if(this->description.compare(r->getDescription()) != 0){
		flag = false;
	}else if(this->exit != r->isExit()){
		flag = false;
	}else if(this->locked != r->isLocked()){
		flag = false;
	}else if(this->dark != r->isDark()){
		flag = false;
	}
	return flag;
}

/**
 * returns all the agents in the room
 */
set<Agent*> Room::getOccupants(){
 	return this->occupants;
}

/**
 * returns all the containers in the room
 */
set<Container*> Room::getRoomObjs(){
	return this->room_objs;
 }


void Room::addOccupant(Agent* a){
	this->occupants.insert(a);
}

void Room::addContainer(Container* obj){
	//cout << "I'm inserting" << obj << "in room " << this->name << endl;
	this->room_objs.insert(obj);
}


void Room::enter(Agent* a){
	this->occupants.insert(a);
}

void Room::leave(Agent* a){
 	this->occupants.erase(a);
}

void Room::setDesc(string desc){
	description = desc;
}
/*
void printOccupants(){
	for(set<Agent*>::iterator it = this->occupants.begin(); it != this->occupants.end(); ++it){
 		cout <<	(*it)->getName() << endl;
 	}
}
*/
