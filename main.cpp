/*comment*/
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>
#include "room.h"
#include <map>
#include <stack>
#include <cstdlib>
#include <ctime>
#include "player.h"
#include "monster.h"
#include <limits>
#include "item.h"

using namespace std;

void gen_map(map<string, Room*>*, stack<string>);
stack<string> randomize_rooms();
void lock_rooms(map<string, Room*>*);
void darken_rooms(map<string, Room*>*);
void add_room_objs(map<string, Room*>*);
bool player_in_danger(Player*);
void destroy_map(map<string, Room*>*);

const string COMMANDS[] = { "look", "move", "check", "open", "use", "take", "drop"};
const string DIRECTIONS[] = { "north", "south", "east", "west" };

int numPlayers;

int main(){
	string name1,name2,desc1,desc2;
	cout << "Welcome to the game. Can you make it out ALIVE!?!?!" << endl;
	cout<<endl;
	
	srand(time(NULL));
	map<string, Room*> game_map;
	gen_map(&game_map, randomize_rooms());
	map<string,Room*>::iterator it = game_map.begin();
	
	while(true){
	cout<< "How Many Players? <1> or <2>"<<endl;
	cout<<endl;
	if(cin>>numPlayers){
		
	
	cout<<endl;

	if(numPlayers == 2){
		cin.ignore();
		cout<<"Name of first player: "<<endl;
		getline(cin,name1);
		cout<<endl;
		cout<<"Name of second player: "<<endl;
		getline(cin,name2);
		cout<<endl;
		cout<<"Description of first player: "<<endl;
		getline(cin,desc1);
		cout<<endl;
		cout<<"Description of second player: "<<endl;
		getline(cin,desc2);
		cout<<endl;
		
		Player *firstPlayer = new Player(name1,desc1,it->second);

		Player *secondPlayer = new Player(name2,desc2,it->second);

		for(int i = 0;i<4;i++){
			++it;
		}
		Monster *SlenderMan1 = new Monster("SlenderMan1",
		"A long time legend that haunts all those that dare walk his corridors, he will find you...",
		it->second);

		Monster *SlenderMan2 = new Monster("SlenderMan2",
		"A long time legend that haunts all those that dare walk his corridors, he will find you...",
		it->second);

		Monster *SlenderMan3 = new Monster("SlenderMan3",
		"A long time legend that haunts all those that dare walk his corridors, he will find you...",
		it->second);

		while(true){
			cout<<firstPlayer->getName()<<", it is your turn!"<<endl;
			cout<<endl;
			firstPlayer->act();
			cout<<secondPlayer->getName()<<", it is your turn!"<<endl;
			cout<<endl;
			secondPlayer->act();
			cout<<"A distant sound of moaning, you know he is near!"<<endl;

			SlenderMan1->act();
			SlenderMan2->act();
			SlenderMan3->act();

			if(player_in_danger(firstPlayer)){
				string choice;
				cin.ignore();
				cout << firstPlayer->getName() << " " << "You are in grave danger from the slenderman!" << endl;
				cout << "Will you fight or flee? Choose wisely my friend!" << endl;
				getline(cin, choice);
				if(choice == "figtht"){
					int num = rand() % 8;
					if(num == 1){
						cout << "You died!! You lose! Thanks for playing" << endl;
						exit(0);
					}else{
						cin.ignore();
						cout << "You can't kill the slenderman! Run!" << endl;
						cout << "Enter a direction" << endl;
						getline(cin, choice);
						Room* loc = firstPlayer->getLocation();
						Room* linked = loc->getLinked(choice);
						if(linked != NULL && !linked->isLocked()){
							loc->leave(firstPlayer);
						      	 linked->enter(firstPlayer);
						}
					}
				}else if(choice == "flee"){
						cin.ignore();
						cout << "Enter a direction" << endl;
						getline(cin, choice);
						Room* loc = firstPlayer->getLocation();
						Room* linked = loc->getLinked(choice);
						if(linked != NULL && !linked->isLocked()){
							loc->leave(firstPlayer);
						      	 linked->enter(firstPlayer);
						}
				}
			}
		       	if( player_in_danger(secondPlayer)){
				string choice;
				cin.ignore();
				cout << secondPlayer->getName() << " " << "You are in grave danger from the slenderman!" << endl;
				cout << "Will you fight or flee? Choose wisely my friend!" << endl;
				getline(cin, choice);
				if(choice == "figtht"){
					int num = rand() % 8;
					if(num == 1){
						cout << "You died!! You lose! Thanks for playing" << endl;
						exit(0);
					}else{
						cin.ignore();
						cout << "You can't kill the slenderman! Run!" << endl;
						cout << "Enter a direction" << endl;
						getline(cin, choice);
						Room* loc = secondPlayer->getLocation();
						Room* linked = loc->getLinked(choice);
						if(linked != NULL && !linked->isLocked()){
							loc->leave(secondPlayer);
						      	 linked->enter(secondPlayer);
						}
					}	
				}else if(choice == "flee"){
					cin.ignore();
					cout << "Enter a direction" << endl;
					getline(cin, choice);
					Room* loc = secondPlayer->getLocation();
					Room* linked = loc->getLinked(choice);
					if(linked != NULL && !linked->isLocked()){
						loc->leave(secondPlayer);
					      	 linked->enter(secondPlayer);
					}
				}
			}
		}
		
		delete firstPlayer;
		delete secondPlayer;
		delete SlenderMan1;
		delete SlenderMan2;
		delete SlenderMan3;

		break;
		
	}
	else if(numPlayers == 1){
		cin.ignore();
		cout<<"Name of first player: "<<endl;
		getline(cin,name1);
		cout<<endl;
		cout<<"Description of first player: "<<endl;
		getline(cin,desc1);
		cout<<endl;
		Player *firstPlayer = new Player(name1,desc1,it->second);

		for(int i = 0;i<4;i++){
			it = ++it;
		}
		Monster *SlenderMan1 = new Monster("SlenderMan1",
		"A long time legend that haunts all those that dare walk his corridors, he will find you...",
		it->second);

		Monster *SlenderMan2 = new Monster("SlenderMan2",
		"A long time legend that haunts all those that dare walk his corridors, he will find you...",
		it->second);

		Monster *SlenderMan3 = new Monster("SlenderMan3",
		"A long time legend that haunts all those that dare walk his corridors, he will find you...",
		it->second);


		while(true){
			firstPlayer->act();	
			SlenderMan1->act();
			SlenderMan2->act();
			SlenderMan3->act();

			if(player_in_danger(firstPlayer)){
				string choice;
				cout << firstPlayer->getName() << " " << "You are in grave danger from the slenderman!" << endl;
				cout << "Will you fight or flee? Choose wisely my friend!" << endl;
				getline(cin, choice);
				if(choice == "figtht"){
					int num = rand() % 8;
					if(num == 1){
						cout << "You died!! You lose! Thanks for playing" << endl;
						exit(0);
					}else{
						cout << "You can't kill the slenderman! Run!" << endl;
						cout << "Enter a direction" << endl;
						getline(cin, choice);
						Room* loc = firstPlayer->getLocation();
						Room* linked = loc->getLinked(choice);
						if(linked != NULL && !linked->isLocked()){
							loc->leave(firstPlayer);
						      	 linked->enter(firstPlayer);
						}
					}	
				}else if(choice == "flee"){
						cout << "Enter a direction" << endl;
						getline(cin, choice);
						Room* loc = firstPlayer->getLocation();
						Room* linked = loc->getLinked(choice);
						if(linked != NULL && !linked->isLocked()){
							loc->leave(firstPlayer);
						      	 linked->enter(firstPlayer);
						}
				}
			}
		}
		
		delete firstPlayer;
		delete SlenderMan1;
		delete SlenderMan2;
		delete SlenderMan3;

		break;
	}
	else{
		cout<<"Incorrect Input: Try Again"<< endl;
		cout<<endl;
	}
	}
	else{
		cin.clear();
		
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		
		cout<<"Incorrect Input: Try Again"<< endl;
		cout<<endl;
	}
	
}
	
	destroy_map(&game_map);
	return 0;
}

void gen_map(map<string, Room*> *m, stack<string> rms){
	srand(time(NULL));
	int num_dir = sizeof(DIRECTIONS)/sizeof(string);
	int rand_dir;

	(*m)[rms.top()] = new Room(rms.top(), "empty room.");
	Room* r = (*m)[rms.top()];
	rms.pop();
	
	while(!rms.empty()){
		rand_dir = rand() % num_dir;
		if(r->getLinked(DIRECTIONS[rand_dir]) == NULL){
			Room *next_r = new Room(rms.top(), "empty room.");
			r->link(next_r, DIRECTIONS[rand_dir]);
			(*m)[rms.top()] = next_r;
			if(DIRECTIONS[rand_dir] == "north"){
				next_r->link(r, "south");
			}else if(DIRECTIONS[rand_dir] == "south"){
				next_r->link(r, "north");
			}else if(DIRECTIONS[rand_dir] == "east"){
				next_r->link(r, "west");
			}else if(DIRECTIONS[rand_dir] == "west"){
				next_r->link(r, "east");
			} 
			r = next_r;
			rms.pop();
		}else if(r->getLinked(DIRECTIONS[rand_dir]) != NULL){
			r = r->getLinked(DIRECTIONS[rand_dir]);
		}
	}
	lock_rooms(m);
	darken_rooms(m);
	add_room_objs(m);
	
/*
	for(map<string,Room*>::iterator it = m->begin(); it != m->end(); ++it){
		cout << it->first << endl;
		cout << it->second->getName()<< endl;
		cout << it->second->getDescription() << endl;
		cout << "Linked:" << endl;
		it->second->printLinked();
		cout << endl;
	}
*/
	for(map<string,Room*>::iterator it = m->begin(); it != m->end(); ++it){
		if(it->second->getName() == "patio"){
			it->second->setDesc(
			"Humid and damp, there lies nothing but flies and the moist words of a hollow wind... dare to question why? You feel scared, but brave");
		}
		else if(it->second->getName() == "library"){
			it->second->setDesc(
			"With a cold shiver up your spine you realize that you have been in this library before, almost as if this was your destined fate to wonder aimlessly through 'his' maze...");						
		}
		else if(it->second->getName() == "hall"){
			it->second->setDesc(
			"narrow and dark you find yourself humbly fearing the unknowm... What's that sound?!?");
		}
		else if(it->second->getName() == "dining room"){
			it->second->setDesc(
			"Still have an appetite when you see 4 dead family members on the table? Good, because SlenderMan comes for you, enjoy the blood Wine...");
		}
		else if(it->second->getName() == "attic"){
			it->second->setDesc(
			"Creaky and clamy, you make your way up the stairway and find yourself surrounded by... masks, thousands of eirie masks that follow you every which way you go");
		}
		else if(it->second->getName() == "dungeon"){
			it->second->setDesc(
			"Well, it's a dungeon, what do you expect? It's a great thing that you love handcuffs and chainmail, be weary of the executioner, I hear he's a hot head...");						
		}
		else if(it->second->getName() == "living room"){
			it->second->setDesc(
			"You walk in to a quite barren lving quarters, and notice that you are not afraid... You have a moment of peace, all to yourself, just don't bother to turn around, he's always watching...");
								
		}
		else if(it->second->getName() == "study"){
			it->second->setDesc(
			"A small room that has obviously seen better days. Nothing impressive... or maybe it is?");						
		}
		else if(it->second->getName() == "kitchen"){
			it->second->setDesc(
			"Butcher Knives line the walls and this black slime crusts the floor... I wonder who died here, I will not be next!");						
		}
		else if(it->second->getName() == "basement"){
			it->second->setDesc(
			"Flooded with water and loose cables everywhere, better be careful not t-.... What was that sound?");						
		}
		else if(it->second->getName() == "secret passage"){
			it->second->setDesc(
			"Most of the time falling through a small shaft is a bad thing, but in this place, it may be the only thing to keep you alive, and sane.");						
		}
		else if(it->second->getName() == "pantry"){
			it->second->setDesc(
			"Do you think there is food?????? Nope just rats. Well back to eating scraps off my shoe... ");						
		}
		else if(it->second->getName() == "office"){
			it->second->setDesc(
			"Another small room with a picture of a family... But something isn't right, they have no faces, who is SlenderMan?");						
		}
		else if(it->second->getName() == "din"){
			it->second->setDesc(
			"... I feel him near me... I can't take this, why am I here... Who's din is this? It needs some redecoration...");						
		}
		else if(it->second->getName() == "closet"){
			it->second->setDesc(
			"... I need to come out of this closet, I don't know why but I feel like 'he' likes this place...");						
		}
		else if(it->second->getName() == "bathroom"){
			it->second->setDesc(
			"No Toilet paper... -___- FML");						
		}
		else if(it->second->getName() == "bedroom"){
			it->second->setDesc(
			"My bedroom, wait... I am in my house? How did I get here, this can't be happening...<Whimpers>");						
		}
		else if(it->second->getName() == "porch"){
			it->second->setDesc(
			"I can't move: The stillness of the air is paralyzing, I gaze at what would be my freedom with sadness and ultimately defeat.");						
		}
		
		
	}
}

stack<string> randomize_rooms(){
	string rm_names[] = {  "patio", "library", "hall", "dinning room", "attic", "dungeon", "living room", "study", "kitchen", "basement",
				"secret passage", "pantry", "office", "din", "closet", "bathroom", "bedroom", "porch" };

	srand(time(NULL));
	int num_rooms = (sizeof(rm_names)/sizeof(string));
	int rand_num = 0;
	stack<string> rand_rms;

	for(int i = 0; i < num_rooms; ++i){
		rand_num = rand() % num_rooms;	
		rand_rms.push(rm_names[rand_num]);

		for(int j = rand_num; j < num_rooms-1; ++j){
			if(j+1 > num_rooms){
				break;
			}else{
				rm_names[j] = rm_names[j+1];
			}
		}
		num_rooms--;
	}
	return rand_rms;
}

void lock_rooms(map<string, Room*> *m){
	map<string, Room*>::iterator it;
	int i = 0;
	srand(time(NULL));

	for(it = m->end(); it != m->begin(); --it){
		if(it == m->end()){
			it--;
			it->second->setExit(true);
		}
		if(i >= 3){
			int r = rand() % 2;
			if(r == 1){
				it->second->setLocked(true);
			}
		}	
		++i;
	}
}

void darken_rooms(map<string, Room*> *m){
	map<string, Room*>::iterator it;
	int i = 0;
	srand(time(NULL));

	for(it = m->begin(); it != m->end(); ++it){
		if(i >= 3){
			int r = rand() % 2;
			if(r == 1){
				it->second->setDark(true);
			}
		}	
		++i;
	}

}

bool player_in_danger(Player* p){
	Room* rm = p->getLocation();
	set<Agent*> occupants = rm->getOccupants();
	if(occupants.size() > 1){
		set<Agent*>::iterator it;
		for(it = occupants.begin(); it != occupants.end(); ++it){
			if((*it)->getName() == "SlenderMan1" || (*it)->getName() == "SlenderMan2" || (*it)->getName() == "SlenderMan3"){
				return true;
			}
		}
	}else{
		return false;
	}
}

void add_room_objs(map<string, Room*>* m){
	srand(time(NULL));
	string cnt_names[] = { "desk", "dresser", "table", "chess", "open safe", "box", "bookshelf", "cabinet"};
	string itm_names[] = { "skeleton key", "flashlight"};
	int rand_num = 0;
	
	map<string, Room*>::iterator it;
	for(it = m->begin(); it != m->end(); ++it){
		Room *r = it->second;
		string r_name = r->getName();
		if(it == m->begin()){
			it->second->addContainer(new Container(r_name + " safe", "safe", "open safe", new item("skeleton key", "key","unlocks doors")));
		}

		for(int i = 0; i < (sizeof(cnt_names)/sizeof(string)); ++i){
			rand_num = rand() % 2;
			if(rand_num == 1){
				if(cnt_names[i] == "desk"){
					it->second->addContainer(new Container(r_name + " desk", "desk", "Neatly organized desk"));
				}else if(cnt_names[i] == "dresser"){
					it->second->addContainer(new Container(r_name + " dresser", "dresser", "dresser with old clothing inside"));
				} else if(cnt_names[i] == "table"){
					it->second->addContainer(new Container(r_name + " table", "table", "dusty old table"));
				} else if(cnt_names[i] == "chess"){
					it->second->addContainer(new Container(r_name + " chess", "chess", "chess"));
				} else if(cnt_names[i] == "open safe"){
					it->second->addContainer(new Container(r_name + " safe", "safe", "open safe"));
				} else if(cnt_names[i] == "box"){
					it->second->addContainer(new Container(r_name + " box", "box", "old cardboard box"));
				} else if(cnt_names[i] == "bookshelf"){
					it->second->addContainer(new Container(r_name + " bookshelf", "bookshelf", "bookshelf with old book on it"));
				} else if(cnt_names[i] == "cabinet"){
					it->second->addContainer(new Container(r_name + " cabinet", "cabinet", "Neatly organized cabinet"));
				}
			}

		}	
	}
}

void destroy_map(map<string, Room*>* m){
	map<string, Room*>::iterator it;
	for(it = m->begin(); it != m->end(); ++it){
		delete it->second;
	}
}
