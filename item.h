#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <iostream>
#include <cstdlib>


using namespace std;

/**
 * Class item
 *
 * represents items that can be found and used 
 */
class item{
	public:
		
		item(){
			Name = "unknown";
			Type = "unknown";
			Description = "unkown object to us and completely irrelevant";
			exists = false;
		}
		item(string iName){
			Name = iName;
			Type = "unknown";
			Description = "unkown object to us and completely irrelevant";
			exists = true;
		}
		item(string iName,string iType){
			Name = iName;
			Type = iType;
			Description = "unkown object to us and completely irrelevant";
			exists = true;
		} 

/**
 * Constructor that sets name type and description
 */
		item(string iName,string iType,string iDesc){
			Name = iName;
			Type = iType;
			Description = iDesc;
			exists = true;
		}
		
		bool getExists(){
			return exists;
		}
		string getName(){
			return Name;
		}
		string getType(){
			return Type;
		}
		string getDesc(){
			return Description;
		}
		
		
		
		
	private:
		string Name;
		string Type;
		string Description;
		bool exists;
};

#endif














