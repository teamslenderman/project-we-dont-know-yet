#ifndef AGENT_H
#define AGENT_H

#include <string>
#include "room.h"
using namespace std;

/**
 * Class Agent - .h-file
 * 
 * Is used as template for Player and Monster
 * Has the properties name, description and the Room-pointer location
 * Has getters and setters for those properties plus a method called act that defines the actions of the Agent:w
 * 
 */

class Agent{
	public:
		string getName();
		void setName(string name);
		string getDescription();
		void setDescription(string description);
		Room* getLocation();
		void setLocation(Room* location);
		
		virtual bool act();

	protected:
		string name;
		string description;
		Room* location;
};
#endif
