#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include "agent.h"
#include "room.h"
#include "Container.h"
#include "inventory.h"

using namespace std;

/**
 * Class Player - .h-file
 *
 * represents a player
 *
 * inherits the Agent class 
 * offers an empty constructor and one that sets all the properties
 * offers a destructor
 * overrides the act() method
 * has an Inventory property
 *
 */

class Player : public Agent{
	public:
		Player();
		Player(string name, string description, Room* location);
		~Player();
		bool act();
		
	private:
		inventory *myInv;
			
};
#endif
