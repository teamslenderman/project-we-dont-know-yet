#include "player.h"
#include <string>
#include <stdlib.h>

/**
 * Player class - .cpp-file
 *
 * Implements the methods for the player
 *
 */


using namespace std;

/**
 * String constant Commands
 * defines the possible options for inputs to start
 */	
const string COMMANDS[] = { "look", "move", "check", "open", "use", "take", "drop" };

Player::Player(){
}

/**
 * Constructor that defines the properties, enters the player to the room and creates a new inventory
 */
Player::Player(string name, string description, Room* location){
	this->name=name;
	this->description=description;
	this->location=location;
	location->enter(this);
	myInv = new inventory("Satchel","bag",
	"Was awaiting me on the floor when I first arrived... it seems to be worn down");
}

/**
 * Destructor
 */
Player::~Player(){
	//TODO
}

/**
 * act() Method 
 * takes commands as user inputs
 * decides what to do according to the command *
 */		
bool Player::act(){
		string decision;
		Agent *tempAgent;
		Container *tempCont;
		set<Container*> c = location->getRoomObjs();
		int n = 0;
		cout<<location->getName()<<" : "<<location->getDescription()<<endl;
		cout<<endl;
		while(true){
		cout<<"What would you like to do?"<<endl;
		cout<<endl;
		cin>>decision;
		cout<<endl;
		if(decision == "look"){
			if(location->isDark() == false){	
				cout<<location->getDescription()<<endl;
				cout<<endl;
				cout<<"There seems to be more entities in here: "<<endl;
				for(set<Agent*>::iterator it = location->getOccupants().begin();it != location->getOccupants().end();++it){
					tempAgent = (*it);
					cout<<tempAgent->getName()<<endl;;
				}
				cout<<endl;
				cout<<"There are objects in here I can maybe check out:"<<endl;
				for(set<Container*>::iterator itt = c.begin();itt != c.end();++itt){
					tempCont = (*itt);
					cout<<tempCont->getName()<<endl;
				}
			}
			else{
				cout<<"Room is dark"<<endl;
			}
			cout<<endl;
		
		}
		else if(decision == "use"){
			string itemTBU;
			cin.ignore();
			cout<<"What Item would you like to use?"<<endl;
			cout<<endl;
			myInv->seeItems();
			cout<<endl;
			getline(cin,itemTBU);
			if(myInv->pickUpItem(itemTBU)->getType() == "flashlight"){
				location->setDark(false);
			}
			myInv->use(myInv->pickUpItem(itemTBU));

			
		}
		else if(decision == "take"){
			string itemTBA;
			cin.ignore();
			cout<<"What Item would you like to take?"<<endl;
			cout<<endl;
			getline(cin,itemTBA);
			for(set<Container*>::iterator itt = c.begin();itt != c.end();++itt){
					tempCont = (*itt);
					myInv->addItem(tempCont->pickUpItem(itemTBA));
					tempCont->removeItem(itemTBA);
						break;
					}
					
				}
		else if(decision == "drop"){
			string itemTBD;
			cin.ignore();
			cout<<"What Item would you like to drop?"<<endl;
			cout<<endl;
			myInv->seeItems();
			cout<<endl;
			getline(cin,itemTBD);
			if(myInv->removeItem(itemTBD))
			{
				cout << "Removed." << endl;
				cout<<endl;
			}
			else
			{
				cout << "Not found." << endl;
				cout<<endl;
			}
		}
		else if(decision == "move"){
			string direction;
			cin.ignore();
			cout<<"Which Direction would you like to go?"<<endl;
			cout<<endl;
			cin>>direction;
			cout<<endl;
			Room* linked = location->getLinked(direction);
			if(linked != NULL && !linked->isLocked()){
				location->leave(this);
				location = location->getLinked(direction);
				location->enter(this);	
				if(location->isExit() && myInv->pickUpItem("skeleton key") != NULL){
					cout << "you've reacked the exit!!!! you win" << endl;
				}
			}else if(linked != NULL && linked->isLocked() && myInv->isEmpty()){
				cout << "This door is locked and you do not have the key!" << endl;
			}else if(linked != NULL && linked->isLocked() && !myInv->isEmpty()){
				if(myInv->pickUpItem("skeleton key") != NULL){
					cout << "You have the key....Door is unlocked!" << endl;
					linked->setLocked(false);
					location->leave(this);
					location = location->getLinked(direction);
					location->enter(this);	
					if(location->isExit()){
					cout << "you've reacked the exit!!!! you win" << endl;
				}
	
				}else{ 
					cout << "The door is lock" << endl;
				}
			}else{
				cout << "There is no door in this direction" << endl;
			}
			
			break;
		}
		else if(decision == "check"){
			string contTBC;
			cin.ignore();
			cout<<"Which object would you like to check?"<<endl;
			cout<<endl;
			for(set<Container*>::iterator itt = c.begin();itt != c.end();++itt){
					tempCont = (*itt);
					cout<<tempCont->getName()<<endl;
				}
			cout<<endl;
			getline(cin,contTBC);
			cout<<endl;
			if(contTBC == "inventory"){
				if(myInv->isEmpty()){
					cout << "Your inventory is empty" << endl;
				}else{
					myInv->seeItems();
				}
			}
			for(set<Container*>::iterator itt = c.begin();itt != c.end();++itt){
					tempCont = (*itt);
					if(tempCont->getName() == contTBC){
						tempCont->seeItems();
						break;
					}
				}
				cout<<endl;
				break;
		}
		else if(decision == "open"){
			string contTBC;
			cout<<"Which object would you like to open?"<<endl;
			cout<<endl;
			/*having problems with this for loop*/
			for(set<Container*>::iterator itt = c.begin();itt != c.end();++itt){
					tempCont = (*itt);
					cout<<tempCont->getName()<<endl;
				}
			cout<<endl;
			getline(cin,contTBC);
			cout<<endl;
			for(set<Container*>::iterator itt = c.begin();itt != c.end();++itt){
					tempCont = (*itt);
					if(tempCont->getName() == contTBC){
						tempCont->seeItems();
						break;
					}
				}
				cout<<endl;
				break;
		}
		else if(decision == "quit"){
			exit(0);
		}
		else if(decision == "room"){
			cout<<location->getName()<< " : "<< location->getDescription()<<endl;
			cout<<endl;
		}
		else{
			cout<<"Not a Valid Command"<<endl;
			cout<<endl;
		}
	}
	}
