#ifndef INVENTORY_H
#define INVENTORY_H

#include <string>
#include <iostream>
#include <cstdlib>
#include "Container.h"

using namespace std;

class inventory : public Container{
	
	public:
	
	inventory()
	:Container()
	{
		isAddable = true;
		
	}
	inventory(string _invName)
	:Container(_invName)
	{
		isAddable = true;
		
	}
	inventory(string _invName,string _invType)
	:Container(_invName,_invType)
	{
		isAddable = true;
		
	}
	inventory(string _invName,string _invType,string _invDesc)
	:Container(_invName,_invType,_invDesc)
	{
		isAddable = true;
		
	}
	inventory(string _invName,string _invType,string _invDesc,item* inv[])
	:Container(_invName,_invType,_invDesc,inv)
	{
		isAddable = true;
		
	}
	
	bool getIsAddable(){
		return isAddable;
	}
	
	
	
	private:
		bool isAddable;
};
#endif
