#ifndef MONSTER_H
#define MONSTER_H

#include <string>
#include "room.h"
#include "agent.h"

/**
 * Class Monster - .h-file
 *
 * represents a Monster
 *
 * inherits the Agent class 
 * offers an empty constructor and one that sets all the properties
 * offers a destructor
 * overrides the act() method
 */

using namespace std;

	

class Monster : public Agent{
	public:
		Monster();
		Monster(string name, string description, Room* location);
		~Monster();
		bool act();
		
	private:
			
};
#endif
